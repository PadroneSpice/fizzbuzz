#include <iostream>

using namespace std;

/**
 *  Programming Challenge: fizzbuzz
 *  Sean Castillo
 *  30 March 2019
 */

int main()
{
    unsigned int numberToCount;
    cout << "Count to what number?" << endl;

    while (1)
    {
        if (cin >> numberToCount) { break; }
        else
        {
            cout << "That's not a valid number! Try again!" << endl;
            cin.clear();
            cin.ignore(100000, '\n');
        }
    }

    for (unsigned int i=1; i<numberToCount+1; i++)
    {
        if (i % 3 == 0 && i % 5 == 0) { cout << "fizzbuzz" << endl; }
        else if (i % 3 == 0)          { cout << "fizz"     << endl; }
        else if (i % 5 == 0)          { cout << "buzz"     << endl; }
        else                          { cout << i          << endl; }
    }

    return 0;
}
